"""info commands and stuff"""
import json
import logging

from urllib import request
from urllib.request import urlopen

import discord
from discord.ext import commands

# pylint: disable=W1202
# pylint: disable=W0703

#logger stuff yeah
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)
LOGGER_HANDLER = logging.FileHandler(filename=__name__ + ".log", encoding='utf-8', mode='w')
LOGGER_HANDLER.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
LOGGER.addHandler(LOGGER_HANDLER)

class Info:
    """the actual info commands' code!!!"""

    def __init__(self, bot):
        self.bot = bot

    async def __after_invoke(self, ctx):
        LOGGER.info('{0.command} is done...'.format(ctx))

    @commands.command()
    async def libs(self, ctx):
        """the libraries used in Nagisa"""
        color = discord.Colour.green()
        with open("nagisa.json") as file:
            data = json.load(file)
            desc = data['libs']
        titi = discord.Embed(title="Libraries used in Nagisa.",
                             description="Nagisa was made with the help of:\n" + desc,
                             colour=color)
        await ctx.send(embed=titi)

    @commands.command()
    async def version(self, ctx):
        """outputs the Nagisa bot version"""
        with open("config.json") as file:
            data = json.load(file)
            ver = data['version']
        await ctx.send("Nagisa - version " + ver + ".")

    @commands.command()
    async def userinfo(self, ctx, user: discord.Member):
        """info about a user (account created at, joined the server at, avatar, etc...)"""
        info = {"Name:": user.name,
                "Avatar:": user.avatar_url,
                "Bot:": str(user.bot),
                "Created at:": str(user.created_at) + " ",
                "Joined at:": str(user.joined_at) + " ",
                "Game:": str(user.game),
                "Nickname": str(user.nick),
                "Highest role:": str(user.top_role)}
        embed = discord.Embed(title=info["Name:"], colour=user.colour)
        embed.set_image(url=info["Avatar:"])
        for key in info:
            if key in ["Name:", "Avatar:"]:
                continue
            val = info[key]
            embed.add_field(name=key, value=val)
        await ctx.send(embed=embed)

    @commands.command()
    async def serverinfo(self, ctx):
        """info about the server (created at, icon, owner, etc..."""
        info = {
            "Created at:": str(ctx.message.guild.created_at) + " ",
            "Name:": str(ctx.message.guild.name),
            "Owner:": str(ctx.message.guild.owner),
            "Verification Level:": str(ctx.message.guild.verification_level),
            "Icon:": str(ctx.message.guild.icon_url),
            "Role count:": len(ctx.message.guild.roles),
            "Emoji count:": len(ctx.message.guild.emojis),
            "Channel count:": len(ctx.message.guild.channels),
            "Region:": str(ctx.message.guild.region),
            "Member count:": ctx.message.guild.member_count,
        }
        embed = discord.Embed(title=info["Name:"], colour=ctx.message.guild.owner.colour)
        embed.set_image(url=info["Icon:"])
        for key in info:
            if key in ["Name:", "Icon:"]:
                continue
            val = info[key]
            embed.add_field(name=key, value=val)
        await ctx.send(embed=embed)

    @commands.command()
    async def ow(self, ctx, exa: str, region: str):
        """Syntax: n!ow user#id eu/us/kr"""
        exa = exa.replace("#", "-")
        region = region.lower()
        url = "http://owapi.net/api/v3/u/" + exa + "/blob"
        LOGGER.info("ow api url: %s", url)
        req = request.Request(url, headers={'User-Agent': "xxl sakujes browser"})
        with urlopen(req) as url:
            data = json.loads(url.read().decode())
        info = {
            "Name:": exa.replace("-", "#"),
            "Avatar:": data[region]["stats"]["quickplay"]["overall_stats"][str("avatar")],
            "**Region: **": region.upper(),
            "**Level: **": data[region]["stats"]["quickplay"]["overall_stats"][str("level")],
            "**Rank: **": data[region]["stats"]["quickplay"]["overall_stats"]["tier"],
            "**Quick Play Wins: **": data[region]["stats"]["quickplay"]
                                     ["overall_stats"][str("wins")],
            "**Quick Play Losses: **": data[region]["stats"]["quickplay"]
                                       ["overall_stats"][str("losses")],
            "**Quick Play Eliminations: **": data[region]["stats"]["quickplay"]
                                             ["game_stats"][str("eliminations")],
            "**Quick Play Deaths: **": data[region]["stats"]["quickplay"]
                                       ["game_stats"][str("deaths")],
            "**Quick Play KDR: **": str(data[region]["stats"]["quickplay"]["game_stats"]
                                        ["eliminations"] / data[region]["stats"]
                                        ["quickplay"]["game_stats"]["deaths"]),
            }
        if data[region]["stats"]["competitive"] is not None:
            exadee = {
                "**Competitive Eliminations: **": data[region]["stats"]
                                                  ["competitive"]["game_stats"]
                                                  [str("eliminations")],
                "**Competitive Deaths: **": data[region]["stats"]
                                            ["competitive"]["game_stats"][str("deaths")],
                "**Competitive Wins: ** ": data[region]["stats"]
                                           ["competitive"]["overall_stats"][str("wins")],
                "**Competitive Losses: ** ": data[region]["stats"]
                                             ["competitive"]["overall_stats"][str("losses")],
                "**Competitive KDR: **": str(data[region]["stats"]
                                             ["competitive"]["game_stats"]["eliminations"] /
                                             data[region]["stats"]["competitive"]
                                             ["game_stats"]["deaths"]),
                }
            xxl = {**info, **exadee}
        else:
            xxl = info
        try:
            if data[region]["stats"]["competitive"]["rank"] is not None or data[region]["stats"]["competitive"] is not None:
                xxl["**Rank: **"] = xxl["**Rank: **"].title()
            else:
                xxl["**Rank: **"] = "None"
        except Exception:
            if data[region]["stats"]["competitive"] is not None:
                xxl["**Rank: **"] = xxl["**Rank: **"].title()
            else:
                xxl["**Rank: **"] = "None"

        embed = discord.Embed(title=xxl["Name:"], colour=ctx.message.author.colour)
        embed.set_image(url=xxl["Avatar:"])
        link = ""

        if xxl["**Rank: **"] == "Bronze":
            link = "http://i.imgur.com/tHld9yz.png"
        elif xxl["**Rank: **"] == "Silver":
            link = "http://i.imgur.com/IOt3CTv.png"
        elif xxl["**Rank: **"] == "Gold":
            link = "http://i.imgur.com/Q9wGr6d.png"
        elif xxl["**Rank: **"] == "Platinum":
            link = "http://i.imgur.com/RrPCNM1.png"
        elif xxl["**Rank: **"] == "Diamond":
            link = "http://i.imgur.com/fkw2cxN.png"
        elif xxl["**Rank: **"] == "Master":
            link = "http://i.imgur.com/HPbH7uV.png"
        elif xxl["**Rank: **"] == "Grandmaster":
            link = "http://i.imgur.com/xdPsP4w.png"

        embed.set_thumbnail(url=link)

        for key in xxl.keys():
            if key in ["Name:", "Avatar:"]:
                continue
            val = xxl[key]
            embed.add_field(name=key, value=val)
        print(xxl)
        await ctx.send(embed=embed)


def setup(bot):
    """adds the cog as a cog"""
    bot.add_cog(Info(bot))
