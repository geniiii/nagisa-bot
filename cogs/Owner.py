"""owner commands!!!"""
import logging
from discord.ext import commands

# pylint: disable=W1202

#logger stuff yeah
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)
LOGGER_HANDLER = logging.FileHandler(filename=__name__ + ".log", encoding='utf-8', mode='w')
LOGGER_HANDLER.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
LOGGER.addHandler(LOGGER_HANDLER)

class Owner:
    """owner command!!!"""
    def __init__(self, bot):
        self.bot = bot

    async def __after_invoke(self, ctx):
        LOGGER.info('{0.command} is done...'.format(ctx))

    @commands.command()
    async def say(self, ctx, *, exa: str):
        """says stuff in chat"""
        if ctx.message.author.id == 149493665591590912:
            await ctx.send(exa)
        else:
            await ctx.send("diki")

    @commands.command()
    async def saytts(self, ctx, *, exa: str):
        """says stuff in chat (tts)"""
        if ctx.message.author.id == 149493665591590912:
            await ctx.send(exa, tts=True)
        else:
            await ctx.send("diki")


def setup(bot):
    """adds the cog as a cog"""
    bot.add_cog(Owner(bot))
    