"""woah!!! voice commands for nagisa"""
import shutil
import sys
import os
import logging
from ctypes.util import find_library
import discord
from discord.ext import commands
import discord.voice_client
import youtube_dl

# pylint: disable=W1202

#cog logger stuff yeah
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)
LOGGER_HANDLER = logging.FileHandler(filename=__name__ + ".log", encoding='utf-8', mode='w')
LOGGER_HANDLER.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
LOGGER.addHandler(LOGGER_HANDLER)

class LogThing(object):
    """logger for yt_dl"""
    @staticmethod
    def debug(msg):
        """woops it died"""
        LOGGER.debug(msg)
    @staticmethod
    def warning(msg):
        """woops it threw a warning"""
        LOGGER.warning(msg)
    @staticmethod
    def error(msg):
        """woops it died"""
        LOGGER.error(msg)

def ayylmao(data):
    """shows the progress that yt_dl has done"""
    if data['status'] == 'finished':
        LOGGER.info("Done downloading, now converting...")


class Experimental:
    """music commands!!!"""
    async def __after_invoke(self, ctx):
        LOGGER.info('{0.command} is done...'.format(ctx))

    ydl_opts = {
        'default_search': 'auto',
        'format': 'bestaudio/best',
        'outtmpl': 'music/%(title)s.%(ext)s',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '128',
        }],
        'LOGGER': LogThing(),
        'progress_hooks': [ayylmao],
    }

    def __init__(self, bot):
        self.bot = bot
        self.players = {}
        self.thingy = os.path.dirname(os.path.realpath('__file__'))

    @commands.command()
    async def playyt(self, ctx, *, exa: str):
        """plays music from youtube"""
        opus_path = os.path.join(self.thingy, "libs/libopus.dll")
        if os.system is "nt":
            discord.opus.load_opus(opus_path)
        elif os.system is "posix" and sys.platform is not "Darwin":
            try:
                opus_path = find_library("opus")
            except Exception:
                errmsg1 = "Looks like I failed to load the library."
                errmsg2 = "If you're on BSD or Linux, make sure you have Opus installed."
                print(errmsg1)
                print(errmsg2)
                LOGGER.error(errmsg1)
                LOGGER.error(errmsg2)
        elif sys.platform is "Darwin":
            macoserrmsg = "Darwin/macOS is not supported yet."
            print(macoserrmsg)
            LOGGER.error(macoserrmsg)
        if ctx.message.author.voice:
            channel = ctx.message.author.voice.channel
        else:
            ctx.send("join a voice channel first")
        guild = ctx.message.guild
        if guild.voice_client is not None:
            nagisa_vc = guild.voice_client
            if nagisa_vc.is_playing:
                nagisa_vc.stop()
        else:
            nagisa_vc = await channel.connect()
        with youtube_dl.YoutubeDL(self.ydl_opts) as ydl:
            ydl.download([exa])
            info_dict = ydl.extract_info(exa, download=False)
            video_title = info_dict.get('title', None)
        if video_title is not None:
            video_title = video_title.replace("|", "_")
            video_title = video_title.replace("/", "_")
            video_title = video_title.replace('"', "'")
        source = discord.FFmpegPCMAudio("music/" + str(video_title) + ".mp3")
        source = discord.PCMVolumeTransformer(source)
        nagisa_vc.play(source)
        self.players.update({ctx.message.guild.id: nagisa_vc})

    @commands.command()
    async def stop(self, ctx):
        """disconnects from voice channel"""
        guild = ctx.message.guild
        nagisa_vc = guild.voice_client
        await nagisa_vc.disconnect()
        self.players.pop(ctx.message.guild.id)

    @commands.command()
    async def clean(self):
        """cleans music directory (owner only)"""
        shutil.rmtree('D:/nagisa/music')
        os.mkdir("D:/nagisa/music")

    @commands.command()
    async def volume(self, ctx, diki: float):
        """changes volume"""
        guild = ctx.message.guild
        nagisa_vc = guild.voice_client
        nagisa_vc.source.volume = diki

    @commands.command()
    async def pause(self, ctx):
        """pauses the music"""
        guild = ctx.message.guild
        nagisa_vc = guild.voice_client
        await nagisa_vc.pause()

    @commands.command()
    async def unpause(self, ctx):
        """unpauses the music"""
        guild = ctx.message.guild
        nagisa_vc = guild.voice_client
        await nagisa_vc.unpause()


def setup(bot):
    """adds the cog as a cog"""
    bot.add_cog(Experimental(bot))
