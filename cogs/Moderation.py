"""moderation commands!!!"""
import logging
import discord
from discord.ext import commands

# pylint: disable=W1202

#logger stuff yeah
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)
LOGGER_HANDLER = logging.FileHandler(filename=__name__ + ".log", encoding='utf-8', mode='w')
LOGGER_HANDLER.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
LOGGER.addHandler(LOGGER_HANDLER)

class Moderation:
    """moderation commands, require permissions"""

    def __init__(self, bot):
        self.bot = bot

    async def __after_invoke(self, ctx):
        LOGGER.info('{0.command} is done...'.format(ctx))

    @commands.command()
    async def kick(self, ctx, member: discord.Member):
        """kicks people"""
        if ctx.message.author.guild_permissions.kick_members:
            try:
                await ctx.kick(member)
                await ctx.send("Done.")
            except discord.Forbidden:
                await ctx.send("I don't have permission to kick any members.")

        else:
            await ctx.send("You don't have permission to kick any members.")

    @commands.command()
    async def ban(self, ctx, member: discord.Member):
        """bans people"""
        if ctx.message.author.guild_permissions.ban_members:
            try:
                await ctx.ban(member)
                await ctx.send("Done.")
            except discord.Forbidden:
                await ctx.send("I don't have permission to kick any members.")
        else:
            await ctx.send("You don't have permission to kick any members.")

    @commands.command()
    async def purge(self, ctx, member: discord.Member, limit: int):
        """removes messages from a user
           syntax: n!purge USER MESSAGE_NUMBER"""
        def is_member(mess):
            """bad docstring"""
            return mess.author == member
        if ctx.message.author.guild_permissions.manage_messages is True:
            try:
                deleted = await ctx.message.channel.purge(limit=limit, check=is_member)
                await ctx.send('Deleted {} message(s) from this channel.'.format(len(deleted)))
            except discord.Forbidden:
                await ctx.send("I have no permission to remove any messages. (Manage messages)")
        else:
            await ctx.send("You have no permissions to remove any messages. (Manage messages)")


def setup(bot):
    """adds the cog as a cog"""
    bot.add_cog(Moderation(bot))
    