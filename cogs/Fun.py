"""fun commands!!! weaduysagd"""
import logging
import base64
import json
from urllib.request import urlretrieve
from urllib.request import urlopen
from discord.ext import commands
import discord

# pylint: disable=W1202

#logger stuff yeah
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)
LOGGER_HANDLER = logging.FileHandler(filename=__name__ + ".log", encoding='utf-8', mode='w')
LOGGER_HANDLER.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
LOGGER.addHandler(LOGGER_HANDLER)

class Fun:
    # pylint: disable=R0904
    # pylint: disable=C0330
    """random commands"""
    def __init__(self, bot):
        self.bot = bot

    async def __after_invoke(self, ctx):
        LOGGER.info('{0.command} is done...'.format(ctx))

    @commands.command()
    async def repeat(self, ctx, *, mess: str, times: int):
        """Repeats a message multiple times."""
        for i in range(times):
            await ctx.send(mess)

    @commands.command()
    async def emojify(self, ctx, *, thingy):
        """shamelessly stolen from Lunar and Supersebi3"""
        message = ""
        for exa in thingy:
            if exa in "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ":
                if exa != " ":
                    message = "{}{}".format(message, exa.replace(
                        exa, ":regional_indicator_{}:".format(exa).lower()))
        await ctx.send(message)

    @commands.command()
    async def decode(self, ctx, mess: str, base: str):
        """decodes stuff in base64, base16, base32, base85"""
        if base == "base64":
            exa = base64.b64decode(mess.encode('utf-8'))
            await ctx.send(exa.decode())
        if base == "base16":
            exa = base64.b16decode(mess.encode('utf-8'))
            await ctx.send(exa.decode())
        if base == "base32":
            exa = base64.b32decode(mess.encode('utf-8'))
            await ctx.send(exa.decode())
        if base == "base85":
            exa = base64.b85decode(mess.encode('utf-8'))
            await ctx.send(exa.decode())

    @commands.command()
    async def encode(self, ctx, mess: str, base: str):
        """encodes stuff in base64, base16, base32, base85"""
        if base == "base64":
            exa = base64.b64encode(mess.encode('utf-8'))
            await ctx.send(exa.decode())
        if base == "base16":
            exa = base64.b16encode(mess.encode('utf-8'))
            await ctx.send(exa.decode())
        if base == "base32":
            exa = base64.b32encode(mess.encode('utf-8'))
            await ctx.send(exa.decode())
        if base == "base85":
            exa = base64.b85encode(mess.encode('utf-8'))
            await ctx.send(exa.decode())

    @commands.command()
    async def cat(self, ctx):
        """sends an image from random.cat"""
        url = "http://random.cat/meow"
        with urlopen(url) as url:
            data = json.loads(url.read().decode())
        lenxdxd = data["file"][-3:]
        urlretrieve(data["file"], "file." + lenxdxd)
        exa = discord.File(fp="file." + lenxdxd)
        await ctx.send(file=exa)


def setup(bot):
    """adds the cog as a cog"""
    bot.add_cog(Fun(bot))
