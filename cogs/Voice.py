"""experimental voice commands will be moved here whenever they're stable"""
import discord
import glob
import asyncio
import youtube_dl
import time
import os
from discord.ext import commands
import discord.voice_client


class log(object):
    def debug(self, msg):
        print(msg)

    def warning(self, msg):
        print(msg)

    def error(self, msg):
        print(msg)


def ayylmao(d):
    if d['status'] == 'finished':
        print('Done downloading, now converting ...')


class Voice:
    ydl_opts = {
        'default_search': 'auto',
        'format': 'bestaudio/best',
        'outtmpl': 'music/%(title)s.%(ext)s',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '128',
        }],
        'logger': log(),
        'progress_hooks': [ayylmao],
    }

    def __init__(self, bot):
        self.bot = bot
        self.players = {}

    @commands.command(pass_context=True)
    async def playyt(self, ctx, x: str):
        """plays music from youtube"""
        discord.opus.load_opus('libopus.dll')
        if ctx.message.author.voice:
            channel = ctx.message.author.voice.channel
        else:
            print("fuck")
        vc = await channel.connect()
        with youtube_dl.YoutubeDL(self.ydl_opts) as ydl:
            ydl.download([x])
            info_dict = ydl.extract_info(x, download=False)
            video_title = info_dict.get('title', None)
        vc.play(discord.FFmpegPCMAudio("music/" + str(video_title) + ".mp3"), after=lambda e: print('done', e))
        self.players.update({ctx.message.guild.id: vc})

    @commands.command(pass_context=True)
    async def stop(self, ctx):
        """disconnects from voice channel"""
        guild = ctx.message.guild
        vc = guild.voice_client
        await vc.disconnect()
        self.players.pop(ctx.message.guild.id)

    @commands.command()
    async def clean(self, ctx):
        """cleans music directory (owner only)"""
        if ctx.message.author.id == 149493665591590912:
            files = glob.glob("/music/*")
            for f in files:
                os.remove(f)
            ctx.send("done")

def setup(bot):
    bot.add_cog(Voice(bot))
