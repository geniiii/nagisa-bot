"""uhh bot thing"""
import json

import os
import logging

import discord
from discord.ext import commands

# pylint: disable=W0703
# pylint: disable=W1202

def nagisa(): #the bot's function
    """the main thing"""
    thingy = os.path.dirname('__file__') #location of the bot
    startup_extensions = { #cogs/extensions to load
        "cogs.Fun",
        "cogs.Moderation",
        "cogs.Owner",
        "cogs.Info",
        "cogs.UrbanDictionary",
        "cogs.Experimental",
        "cogs.Geni",
    }

    config = os.path.join(thingy, "config/config.json") #config file

    with open(config) as file: #opens the config file and saves data from it to variables
        data = json.load(file)
        prefix = data['prefix']
        description = data['description']
        token = data['token']
        game = data['game']

	# logging shit
    nagisa_log = logging.getLogger("discord") #makes the logger
    nagisa_log.setLevel(logging.INFO) #sets the logger's level
    handler = logging.FileHandler(filename="nagisa.log", encoding='utf-8', mode='w') #puts the logger in a file (using a handler)
    handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s')) #the log message's format
    nagisa_log.addHandler(handler) #adds the handler

	# the bot (THE BATH XD)
    bot = commands.Bot( #adds the bot
        command_prefix=prefix, #the bot's prefix (n!, for example)
        description=description, #the bot's description in n!help
        game=discord.Game(name=game)) #the game (you can set it in the config file)


    @bot.event
    async def on_ready():
        """does stuff when the bot runs yes""" #pretty much
        with open(config) as file: #loads the config
            conf = json.load(file)
        print("Username: " + bot.user.name)
        print("ID: " + str(bot.user.id))
        print("Prefix: " + str(bot.command_prefix))
        print("Version: " + conf["version"])
        print("Game: " + conf["game"])
        print("Description: " + conf["description"])

    if __name__ == "__main__": #loads the extensions
        for extension in startup_extensions: #looks for extensions
            try:
                bot.load_extension(extension) #tries to load extensions
            except Exception as thing: #if it fails
                exc = "{}: {}".format(type(thing).__name__, thing) #the exception
                msg = "Failed to load extension {}\n{}".format(extension, exc)
                print(msg) #says it failed to load an extension and prints the exception message
                nagisa_log.error(msg) #logs it to the nagisa.log file

    bot.run(token)

if __name__ == "__main__":
    nagisa() #runs the bot
